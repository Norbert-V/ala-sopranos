<?php
require 'assets/inc/header.php';
?>
    <section class="container">
        <div class="c-col col-66">
            <h2 class="heading">Contact</h2>
            <form id="contact" class="form-b">
                <fieldset>
                    <legend>Gegevens</legend>
                    <label for="firstname" class="form-b-label">Voornaam:</label>
                        <input type="text" id="firstname" class="form-b-input" />
                    <label for="surname" class="form-b-label">Achternaam:</label>
                        <input type="text" id="surname" class="form-b-input" />
                    <label for="email" class="form-b-label">Email:</label>
                        <input type="email" id="email" class="form-b-input" />
                </fieldset>
                <fieldset>
                    <legend>Adresgegevens</legend>
                    <label for="city" class="form-b-label">Woonplaats:</label>
                        <input type="text" id="city" class="form-b-input" />
                    <label for="postalcode" class="form-b-label">Postcode:</label>
                        <input type="text" id="postalcode" class="form-b-input" />
                    <label for="adres" class="form-b-label">Huisnummer:</label>
                        <input type="number" id="adres" class="form-b-input" />
                </fieldset>
                <input type="submit" value="Verzenden" class="form-b-submit" />
            </form>

        </div>
        <div class="c-col col-33">
            <h3 class="heading">Rotterdam</h3>
            <p class="text">
                <strong>Sopranos Pizza Rotterdam</strong><br/>
                Lijnbaan 31<br/>
                3012EK, Rotterdam<br/>
                010-5671234<br/>
                rotterdam@sopranos.nl
            </p>
        </div>
        <div class="c-col col-33">
            <h3 class="heading">Amsterdam</h3>
            <p class="text">
                <strong>Sopranos Pizza Amsterdam</strong><br/>
                Kalverstraat 114<br/>
                1012PK, Amsterdam<br/>
                020-9874563<br/>
                amsterdam@sopranos.nl
            </p>
        </div>
        <div class="c-col col-33">
            <h3 class="heading">Utrecht</h3>
            <p class="text">
                <strong>Sopranos Pizza Utrecht</strong><br/>
                Oude Gracht 157<br/>
                3511 AK Utrecht<br/>
                030-2314701<br/>
                utrecht@sopranos.nl
            </p>
        </div>
    </section>
<?php
require 'assets/inc/footer.php';