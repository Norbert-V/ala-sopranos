<?php
require 'assets/classes/menu.php';

new Menu('Pizza Salami', 5,99, 'Met extra salami', 'url');
?>
<!DOCTYPE html>
<html>
<head lang="nl">
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="Norbert Veelenturf">
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="assets/img/favicon.ico" type="image/x-icon">
    <title>Pizza Soprano</title>
    <meta name="viewport" content="width=device-width, user-scalable=0">
    <link rel="stylesheet" href="assets/css/styles.css">
    <link rel="stylesheet" media="(max-width: 990px),(max-device-width: 990px) and (orientation: portrait)" href="assets/css/mobile.css">
</head>
<body>
<header>
    <nav id="main-menu">
        <label for="nav-expander" class="menu" id="nav-label"></label>
        <input type="checkbox" id="nav-expander" />
        <div id="nav-expand">
            <ul>
                <li><a href="index.php">Home</a></li>
                <li><a href="contact.php">Contact</a></li>
                <li><a href="about.php">Over ons</a></li>
                <li><a href="menu.php">Menukaart</a></li>
            </ul>
        </div>

        <label for="shopping-expander" class="menu" id="shopping-label"></label>
        <input type="checkbox" id="shopping-expander" />
        <div id="shop-expand">
        </div>
    </nav>
    <div id="pattern"></div>
    <div id="logo">
        Sopranos
        <span>Sopranos</span>
        <strong>pizza</strong>
    </div>
</header>
