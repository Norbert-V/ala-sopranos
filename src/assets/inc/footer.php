<div class="clear"></div>
<footer>
    <ul class="footer-menu">
        <li class="footer-menu-heading">Rotterdam</li>
        <li>Lijnbaan 31</li>
        <li>3012EK, Rotterdam</li>
        <li>010-5671234</li>
        <li>rotterdam@sopranos.nl</li>
        <li><a href="#">Bekijk kaart >></a></li>
    </ul>
    <ul class="footer-menu">
        <li class="footer-menu-heading">Amsterdam</li>
        <li>Kalverstraat 114</li>
        <li>1012PK, Amsterdam</li>
        <li>020-9874563</li>
        <li>amsterdam@sopranos.nl</li>
        <li><a href="#">Bekijk kaart >></a></li>
    </ul>
    <ul class="footer-menu">
        <li class="footer-menu-heading">Utrecht</li>
        <li>Oude Gracht 157</li>
        <li>3511 AK Utrecht</li>
        <li>030-2314701</li>
        <li>utrecht@sopranos.nl</li>
        <li><a href="#">Bekijk kaart >></a></li>
    </ul>
</footer>
<script type="text/javascript" src="assets/js/randomize.js"></script>
<script type="text/javascript" src="assets/js/order.js"></script>
</body>
</html>