var i;

var pizza = [
    {
        'name' : 'Pizza Margherita',
        'price' : '9,00',
        'image' : 'margarita'
    },
    {
        'name' : 'Pizza Hawaii',
        'price' : '9,50',
        'image' : 'hawaii'
    },
    {
        'name' : 'Pizza Napoletana',
        'price' : '9,50',
        'image' : 'napoletana'
    },
    {
        'name' : 'Pizza Prosciutto',
        'price' : '9,50',
        'image' : 'prosciutto'
    },
    {
        'name' : 'Pizza Borromea',
        'price' : '9,50',
        'image' : ''
    },
    {
        'name' : 'Quattro Formaggi',
        'price' : '10,50',
        'image' : ''
    },
    {
        'name' : 'Pizza Shoarma',
        'price' : '11,50',
        'image' : ''
    }
];

addEventListener('load', randomize);

function randomize() {
    var random = Math.floor(Math.random() * 3);
    for(i = random; i < random + 4; i++) {
        document.getElementById("featured").innerHTML += '<div class="featured-col"><h2 class="heading">' + pizza[i]['name'] + '</h2> <img class="featured-img" src="assets/img/menu/' + pizza[i]['image'] + '.png" alt="" /> <span class="featured-txt">&euro; ' + pizza[i]['price'] + '</span> </div>';
    }
}
