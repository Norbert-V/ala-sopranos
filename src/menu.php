<?php
session_start();
error_reporting(ENT_DISALLOWED);

require 'assets/php/functions.inc.php';
require 'assets/php/pizzas.array.php';
require 'assets/inc/header.php';
?>
    <section class="container">
        <div class="c-col col-100">
            <h2 class="heading">Sopranos menukaart</h2>
            <p class="text">Op deze pagina vind u een uitgebreid overzicht van alle producten die wij aanbieden in al onze drie vestegingen.
                Neem gerust een kijkje en kies zelf welke overheerlijke gerechten u wilt bestellen van ons menu!</p>
        </div>
        <div class="c-col col-66">
            <h2 class="heading">Pizza's</h2>
            <?php
            echo showPizzaMenu($pizzas); //maak de menukaart met pizzas vanuit de array
            ?>
        </div>
        <div class="c-col col-33">
            <h2 class="heading">Winkelwagen</h2>
            <?php
            //check action
            if(isset($_POST["afrekenen"]) || isset($_POST["emptycart"])) {
                if(isset($_POST["emptycart"])) {
                    $_SESSION["cart"] = "";
                    //session_destroy();    //kill all sessions
                    unset($_SESSION["cart"]); //netter, alleen cart wordt leeggemaakt
                }
                if(isset($_POST["afrekenen"])) {
                    //todo: afrekenen
                    echo 'afrekenen';
                    exit;
                }
            }

            //check cart session
            $cart = array();
            if(isset($_SESSION["cart"])) $cart = $_SESSION["cart"];

            //check order
            if(isset($_POST["pizza"]) && isset($_POST["size"]) && isset($_POST["aantal"])) {
                $pizza = $_POST["pizza"];
                $size = $_POST["size"];
                $aantal = $_POST["aantal"];
                $price = $pizzas[$pizza][$size]; //get price from input array
                //make order as array
                $order = array($pizza, $size, $aantal, ($price*$aantal), 0 ); //1 = default voor aantal

                $_SESSION["cart"] = addOrderToCart($cart,$order);
            }

            echo showCart($_SESSION["cart"]);
            ?>
        </div>
    </section>
<?php
require 'assets/inc/footer.php';