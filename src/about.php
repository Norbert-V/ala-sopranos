<?php
require 'assets/inc/header.php';
?>
    <section class="container">
        <div class="c-col col-100">
            <h2 class="heading">Pizza Sopranos</h2>
            <p class="text">Pizza Sopranos is in 1901 opgericht door Mario en Luigi Sopranos.</p>
        </div>
        <div class="c-col col-33">
            <h3 class="heading">Rotterdam</h3>
            <img class="snippet-img" src="assets/img/rotterdam.jpg" alt="Sopranos Rotterdam" />
            <p class="text">
                <strong>Sopranos Pizza Rotterdam</strong><br/>
                Lijnbaan 31<br/>
                3012EK, Rotterdam<br/>
                010-5671234<br/>
                rotterdam@sopranos.nl
            </p>
        </div>
        <div class="c-col col-33">
            <h3 class="heading">Amsterdam</h3>
            <img class="snippet-img" src="assets/img/amsterdam.jpg" alt="Sopranos Amsterdam" />
            <p class="text">
                <strong>Sopranos Pizza Amsterdam</strong><br/>
                Kalverstraat 114<br/>
                1012PK, Amsterdam<br/>
                020-9874563<br/>
                amsterdam@sopranos.nl
            </p>
        </div>
        <div class="c-col col-33">
            <h3 class="heading">Utrecht</h3>
            <img class="snippet-img" src="assets/img/utrecht.jpg" alt="Sopranos Utrecht" />
            <p class="text">
                <strong>Sopranos Pizza Utrecht</strong><br/>
                Oude Gracht 157<br/>
                3511 AK Utrecht<br/>
                030-2314701<br/>
                utrecht@sopranos.nl
            </p>
        </div>
    </section>
<?php
require 'assets/inc/footer.php';